const User = require('../models/User');
const parseStringAsArray = require('../utils/parseStringAsArray')

module.exports = {
    async index(request, response){        
        const { latitude, longitude, techs } = request.query;
        //Buscar todos users num raio de 10 km
        //Filtrar por tecnologias
        const techsArray = parseStringAsArray(techs);
        
        const users = await User.find({
            techs: {
                $in: techsArray,
            },
            location: {
                $near: {
                    $geometry: {
                        type: 'Point',
                        coordinates: [longitude, latitude],
                    },
                    $maxDistance: 10000,
                },
            },
        });

        return response.json({ users })
    }
}