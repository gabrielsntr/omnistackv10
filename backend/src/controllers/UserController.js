const axios = require('axios');
const User = require('../models/User')
const parseStringAsArray = require('../utils/parseStringAsArray')

// funções controller: index, show, store, update, destroy

module.exports = {
    async index(request, response){
        const users = await User.find();
        return response.json(users);
    },

    async store(request, response) {
        const { github_username, techs, latitude, longitude } = request.body;
        //await aguarda resposta da api para continuar

        let user = await User.findOne({ github_username });
        
        if (!user){
            const apiResponse = await axios.get(`https://api.github.com/users/${github_username}`);

            //desestrutura json e caso name for null usa o login. Substitui instrução abaixo
            let { name = login, avatar_url, bio } = apiResponse.data;
            //if (!name){
            //    name = apiResponse.data.login;
            //}
        
            const techsArray = parseStringAsArray(techs);
        
            const location = {
                type: 'Point',
                coordinates: [longitude, latitude]
            };
        
        
            user = await User.create({
                name,
                github_username,
                bio,
                avatar_url,
                techs: techsArray,
                location
            });
        };

    return response.json(user);
    },
    
    async update(request, response){
        const { github_username  } = request.params;
        const { name, avatar_url, bio, techs, latitude, longitude } = request.body;
        //await aguarda resposta da api para continuar

        
        let user = await User.findOne({ github_username });
        if (user){ 
            const techsArray = parseStringAsArray(techs);
        
            const location = {
                type: 'Point',
                coordinates: [longitude, latitude]
            };

            user.name = name;
            user.bio = bio;
            user.avatar_url = avatar_url;
            user.techsArray = techsArray;
            user.location = location;

            user.save();
        };
        return response.json(user);
    
    },

    async destroy(request, response){
        const { github_username } = request.params;
        let user = User.findOne({github_username});
        if (user){
            User.deleteOne(user, function (err) { });
            return response.json({res: "Removido com sucesso!"});
        } else {
            return response.json({res: "Registro não encontrado!"});
        }
        
    },

}