// Importa express
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
//Inicia aplicação express
const app = express();
const routes = require('./routes');

mongoose.connect('mongodb+srv://admin:100560@cluster0-v80pw.mongodb.net/test?retryWrites=true&w=majority',
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

//app.get() válido apenas começadas com get
//app.use() válido para todas as rotas da aplicação
app.use(cors({
    origin:"http://localhost:3000"
}))
app.use(express.json());
app.use(routes);

// Define porta
app.listen(3333);