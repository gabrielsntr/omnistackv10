//Importando somente Router da lib express
const { Router } = require('express');
const routes = Router();
const UserController = require('./controllers/UserController');
const SearchController = require('./controllers/SearchController');


// Tipos de parâmetros:

// Query Params: request.query (Filtros, ordenação, paginação, etc.) GET
// Route Params: request.params (Idenfiricar recurso na alteração ou remoção /users/:id ou /users/:nome)
// Body: request.body (Dados para criação ou alteração de um registro)

// Rotas de user. Declara método assincrono
routes.get('/users', UserController.index);
routes.post('/users', UserController.store);
routes.put('/users/:github_username', UserController.update);
routes.delete('/users/:github_username', UserController.destroy);


// Procura por localização
routes.get('/search', SearchController.index);

module.exports = routes;