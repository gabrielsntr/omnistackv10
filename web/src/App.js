import React, { useState, useEffect } from 'react';
import DevItem from './components/DevItem'
import DevForm from './components/DevForm'
import api from './services/api'
import './global.css'
import './App.css'
import './Sidebar.css'
import './Main.css'

/*Conceitos React: 
  Componente: bloco isolado de HTML, CSS e JS, o qual não interfere no restante da aplicação
  Propriedade: atributos ou informações que um componente PAI passa para o componente filho
  Estado: Informações mantidas pelo componente (imutabilidade)

  Para colocar um componente abaixo do outro é necessário utilizar um container div. No react usar fragment <> </>

let [counter, setCounter] = useState(0);

function incrementCounter(){
  setCounter(counter + 1);
}*/


function App() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    async function loadUsers(){
      // eslint-disable-next-line
      const response = await api.get('/users');
      setUsers(response.data);
    }
    loadUsers();
  }, []);

  async function handleAddUser(data){
    // eslint-disable-next-line
    const response = await api.post('/users', data);
    //Por conta do conceito da imutabilidade, cria-se um novo array e adiciona a response ao final ]
    setUsers([...users, response.data])
  }

  return (
    <div id="app">
      <aside>
        <strong>Cadastrar</strong>
        <DevForm onSubmit={handleAddUser}/>
      </aside>
      
      <main>
        <ul>
          {users.map(user => (
            <DevItem key={user._id} user={user} />
          )
          )}
        </ul>
      </main>
    </div>
  );
}

export default App;
