import React from 'react';

import './styles.css'

function DevItem(props){
    const { user } = props;
    return (
        <li className="dev-item">
        <header>
          <img src={user.avatar_url} alt={user.name}/>
          <div className="user-info">
            <strong>{user.name}</strong>
            <span>{user.techs.join(', ')}</span>
          </div>
        </header>
        <p>{user.bio}</p>
        <a href={`https://github.com/${user.github_username}`}>Acessar perfil no Github</a>
      </li>        
    )
}

export default DevItem;